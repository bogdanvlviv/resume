# Resume

## Bogdan Denkovych

https://gitlab.com/bogdanvlviv

### [Ruby on Rails](https://rubyonrails.org)

Rails is a web application development framework written in the Ruby programming language.

#### Contributor

> March 26, 2016 - Present

https://github.com/rails/rails/pulls?q=author%3Abogdanvlviv

### [GitLab](https://about.gitlab.com)

GitLab is the most comprehensive DevSecOps platform.

#### Senior Backend Engineer

> August 1, 2024 - Present

#### Backend Engineer, Authentication and Authorization Group

> September 13, 2021 - July 31, 2024

https://gitlab.com/bdenkovych

https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&state=all&author_username=bdenkovych

https://gitlab.com/groups/gitlab-org/-/issues?sort=created_date&state=all&assignee_username[]=bdenkovych

https://gitlab.com/groups/gitlab-com/-/merge_requests?scope=all&state=all&author_username=bdenkovych

### [Syncro](https://syncromsp.com)

Syncro is a managed service provider platform.

#### Senior Ruby developer

> May 4, 2021 - August 31, 2021

https://gitlab.com/bogdanvlviv/posts/-/issues/26

### [xneelo](https://xneelo.com)

xneelo is a web hosting provider.

#### Senior Ruby developer

> September 23, 2019 - April 16, 2021

https://xneelo.co.za/insights/introducing-our-new-control-panel/

https://xneelo.co.za/help-centre/control-panel/collaborators/

https://xneelo.co.za/help-centre/control-panel/manage-ftp-users/

### [Current RMS](https://www.current-rms.com)

Current RMS is a cloud rental management software.

#### Senior Ruby developer

> May 14, 2018 - August 30, 2019

https://gitlab.com/bogdanvlviv/posts/-/issues/20

https://gitlab.com/bogdanvlviv/posts/-/issues/14

### [STUDLAVA](https://studlava.com)

STUDLAVA helps students in Ukraine find jobs.

#### Senior Ruby developer

> April 4, 2018 - April 27, 2018

Designed and implemented subscription plans.

https://studlava.com/paid_services

### [Peanut Butter](https://www.getpeanutbutter.com)

Peanut Butter helps employers offer student loan assistance as a benefit.

#### Ruby developer

> February 15, 2016 - May 16, 2016
>
> December 2, 2016 - March 21, 2018

### [Vertalab](https://web.archive.org/web/20181128192932/https://vertalab.com/)

Vertalab is a Ruby on Rails development company specializing in turning raw ideas into web applications.

#### Ruby developer

> June 10, 2015 - September 30, 2018

https://gitlab.com/bogdanvlviv/posts/-/issues/4

### Open source

> April 5, 2015 - Present

https://github.com/bogdanvlviv

### Talks

#### [Pivorak 38: How to upgrade a big Rails application](https://gitlab.com/bogdanvlviv/posts/-/issues/20)

> March 22, 2019

#### [Pivorak 28: Rails 5.2](https://gitlab.com/bogdanvlviv/posts/-/issues/10)

> November 24, 2017

### Volunteering

#### [Rails Girls Lviv](https://gitlab.com/bogdanvlviv/posts/-/issues/21)

> December 7, 2019

#### [Rails Girls Lviv](https://gitlab.com/bogdanvlviv/posts/-/issues/19)

> March 30, 2019

#### [Rails Teens](https://gitlab.com/bogdanvlviv/posts/-/issues/17)

> December 8, 2018

#### [Rails Girls Lviv](https://gitlab.com/bogdanvlviv/posts/-/issues/11)

> February 10, 2018

### Education

#### [Ivan Franko National University of Lviv](https://lnu.edu.ua/en/)

##### [Faculty of Applied Mathematics and Informatics](https://ami.lnu.edu.ua/en/)

> September 1, 2013 - June 30, 2017

Bachelor's degree.
